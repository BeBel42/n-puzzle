'use strict';

function getData() {
	return {
		dim: 3,
		invalidText: '',
		output: '',

		get canSend() {
			return this.output != 'Loading...';
		},

		get end() {
			switch (this.dim) {
				case 1:
					return [[0]];
				case 2:
					return [
						[1, 2],
						[0, 3],
					];
				case 3:
					return [
						[1, 2, 3],
						[8, 0, 4],
						[7, 6, 5],
					];
			  default:
				  throw `${this.dim} is not 1 - 3`;
			}
		},

		checkAndSend(e) {
			const gr = [];
			for (let y = 0; y < this.dim; y++) {
				for (let x = 0; x < this.dim; x++) {
					gr.push(Number(document.querySelector(`#c-${x}-${y}`).value));
				}
			}
			const hasDuplicates = (new Set(gr)).size !== gr.length;
			const isInRange = !gr.some(x => (x < 0 || x >= this.dim * this.dim));
			if (hasDuplicates || !isInRange) {
				if (hasDuplicates)
					this.invalidText = `
Grid elements must be unique`;
				else
					this.invalidText = `
Grid elements must be between 0 and ${this.dim * this.dim - 1}`;
				console.warn(this.invalidText);
				return;
			}
			this.invalidText = '';

			this.output = 'Loading...';
			fetch(`./get_output.php?n=${this.dim}&grid=${gr}`)
				.then(res => res.text())
				.then(text => {
					this.output = text;
				});
		},

		sendSolvable() {
			this.output = 'Loading...';
			fetch(`./get_output.php?n=${this.dim}&solvable=true`)
				.then(res => res.text())
				.then(text => {
					this.output = text;
				});
		},

		sendUnsolvable() {
			this.output = 'Loading...';
			fetch(`./get_output.php?n=${this.dim}&solvable=false`)
				.then(res => res.text())
				.then(text => {
					this.output = text;
				});
		},

		updateDim(event) {
			this.dim = Number(event.target.value);
		},

		get getGrid() {
			let r = '<table id="tableid"><tbody>';
			for (let y = 0; y < this.dim; y++) {
				r += '<tr>';
				for (let x = 0; x < this.dim; x++) {
					r += `
			<td style="
				border: 1px solid;
				text-align: center;
				padding-right: 20px;
				background-color: #123;
			">
				<input style="width: 100%;"
					type="number"
					min="0"
					max="${this.dim * this.dim - 1}"
					id="c-${x}-${y}"
					value="${this.end[y][x]}"/>
			</td> `;
				}
				r += '</tr>';
			}
			r += '</tbody></table>';
			return r;
		},
	};
};
