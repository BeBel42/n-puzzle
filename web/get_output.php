<?php

$script_path = getenv('N_PUZZLE_PATH') ?: '../n-puzzle';

if (!isset($_GET['n']))
	exit('Error: missing n parameter');
$n = (int)$_GET['n'];

if ($n <= 0 || $n > 3)
	exit('Error: n must be in range 1-3');

if (isset($_GET['solvable'])) {
	$s = $_GET['solvable'];
	if ($s == 'false')
		$exec_s = $script_path . ' g -u ' . $n;
	else
		$exec_s = $script_path . ' g ' . $n;
	echo $exec_s . "\n";
	echo `{$exec_s}`;
	exit();
}

if (!isset($_GET['grid']))
	exit('Error: missing grid parameter');
$grid = explode(',', $_GET['grid']);
if (sizeof($grid) != $n * $n)
	exit('Error: invalid grid size');

$grid_str = '';
for ($i = 0; $i < $n; $i++) {
	for ($j = 0; $j < $n; $j++)
		$grid_str .= (int)$grid[$n * $i + $j] . "\n";
	$grid_str .= "\n";
}

$command = "
$n
$grid_str";

$exec_s = "echo '$command' | $script_path";
echo $exec_s . "\n";
echo `{$exec_s}`;
