<?php
function display_end_table()
{
	$end = [
		[1, 2, 3],
		[8, 0, 4],
		[7, 6, 5],
	];
	echo '<table><tbody>';
	foreach ($end as $y) {
		echo  '<tr>';
		foreach ($y as $x)
			echo '
				<td style="
					border: 1px solid;
					text-align: center;
					padding-right: 20px;
					background-color: #123;
				">
			'
				. '<input style="width: 100%;" type="number" min="0" max="8" value="' . $x . '"/>'
				. '</td> ';
		echo  '</tr>';
	}
	echo '</tbody></table>';
}

function display_output()
{
	echo '
		<div style="background-color: #222;">
			<pre
				x-text="output"
				style="
				min-height: 50vh;
				max-height: 50vh;
				overflow-y: auto;
				overflow-x: auto;
				padding: 7px;
			">
			</pre>
		</div>
	';
}
?>

<!doctype html>
<html lang="en-US">

<head>
	<script defer src="https://cdn.jsdelivr.net/npm/alpinejs@3.x.x/dist/cdn.min.js"></script>
	<script src="./script.js"></script>
	<meta charset="utf-8">
	<meta http-equiv="x-ua-compatible" content="ie=edge">
	<title>N-Puzzle Web Portal</title>
	<meta name="description" content="Interact with N-Puzzle online!">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/water.css@2/out/dark.css">
	<link rel="icon" type="image/png" href="/favicon.png" />
	<meta name="darkreader-lock">
</head>

<body style='height: 100vh;' x-data='getData()'>
	<div style='display: flex; justify-content: space-between;'>
		<div style="
				display:flex;
				flex-direction: column;
				max-width: 40%;
			">
			<h1><u> N-Puzzle </u></h1>
			<div x-html='getGrid'></div>
			<div style='
				display: flex;
				align-items: center;
						justify-content: space-evenly;
			'>
				<input type='range' min='1' max='3' value='3' @input='updateDim' />
				<div x-text='`${dim}x${dim}`'></div>
			</div>
			<button :disabled='!canSend' type='submit' @click='checkAndSend'>Submit</button>
			<div x-show='invalidText' style='
						background-color: #a00;
						padding: 5px;
						margin: 5px;
						border-radius: 5px;
						'>
				<b x-text='invalidText'></b>
			</div>
			We can also generate the grid for you:
			<div style='display: flex;'>
				<button style='color: gold;' type='submit' :disabled='!canSend' @click='sendSolvable'>Use Solvable</button>
				<button style='color: gold;' type='submit' :disabled='!canSend' @click='sendUnsolvable'>Use Unsolvable</button>
			</div>
		</div>
		<div style='min-width: 50%; max-width: 50%;'>
			<h2>Output:</h2>
			<?php display_output() ?>
		</div>
	</div>
	<hr />
	<p>
		To save CPU usage, N-Puzzle is limited to 3x3.<br />
		Want more? You can
		<a href='https://gitlab.com/BeBel42/n-puzzle'>use it locally.</a><br />
	</p>
	<p>
		mlefevre, pvanderl
	</p>
</body>

</html>
