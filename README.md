# n-puzzle
A program that solves the [Jeu de Taquin](https://en.wikipedia.org/wiki/Jeu_de_taquin)

![thumbnail](https://gitlab.com/BeBel42/n-puzzle/-/raw/main/thumbnail.png)

## Dependencies
`boost`:  
``` shell
apt-get install libboost-all-dev # Debian
pacman -S boost # Arch
brew install boost # MacOS
```
`make`  
`g++`  

## Makefile
To compile n-puzzle:  
``` shell
	make
```

Other make rules exist such as:  
``` shell
	make clean # delete ever binaries / dependencies except executable
	make fclean # clean but deletes executable too
```

## Web portal

You can build and run the docker image from the Dockerfile. The web uses port 8080.  
Alternatively, you can try our [self-hosted web portal](https://n-puzzle.mlefevre.xyz)
