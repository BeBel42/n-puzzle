#ifndef TYPES_H_
#define TYPES_H_

#include <vector>
#include <utility>
#include <unordered_map>
#include <unordered_set>
#include <memory>
#include <queue>
#include <functional>

class AStarNode;

struct Types {
	using DistType = double;
	using GridType = std::vector<std::vector<unsigned>>;
	using EndType = std::unordered_map<unsigned, std::pair<unsigned, unsigned>>;
	using wptr_t = std::weak_ptr<AStarNode>;
	using sptr_t = std::shared_ptr<AStarNode>;
	using PendingNodes = std::priority_queue<
		sptr_t, std::vector<sptr_t>, std::greater<sptr_t>>;
	using Visited = std::unordered_set<std::shared_ptr<AStarNode>>;
	using Heuristic = std::function<Types::DistType(const AStarNode&)>;
	enum class NeighborSides {
		NONE,
		UP,
		DOWN,
		LEFT,
		RIGHT,
	};
};


#endif // TYPES_H_
