#include <optional>
#include <cmath>
#include "heuristics.hpp"
#include "../Types.hpp"
#include "../AStarNode/AStarNode.hpp"

// return hash map representing index -> {x, y} of the ending grid
static const Types::EndType& get_end(unsigned dim) noexcept
{
	static std::optional<Types::EndType> end;
	if (end && dim * dim == end->size()) return *end;
	end = Types::EndType(); // setting end to some value
	auto l = dim; // length of straight line to fill
	auto repeat = true; // should next line be of same size
	// cursor position
	auto x = -1;
	auto y = 0;
	// cursor direction
	auto xdir = 0;
	auto ydir = -1;
	// cell value
	auto index = 1u;

	// fill hash map
	while (index < dim * dim)
	{
		// change direction
		std::swap(xdir, ydir);
		// shoudl line be the same length, or should direction be inverted
		if (!repeat)
			l--;
		else
			xdir *= -1;
		repeat = !repeat;
		// fill straight line
		for (auto i = 0u; i < l; i++) {
			x += xdir;
			y += ydir;
			(*end)[index] = {x, y};
			index++;
		}
	}
	return *end;
}

Types::DistType heuristics::g(const AStarNode& node) noexcept {
	return node.g;
}

Types::DistType heuristics::manhattan(const AStarNode& node) noexcept
{
	const auto end = get_end(node.grid.size());
	auto sum = 0u; // return value
	for (auto y = 0u; y < node.grid.size(); y++)
		for (auto x = 0u; x < node.grid.size(); x++) {
			auto case_number = node.grid[y][x];
			if (case_number == 0) continue;
			decltype(x) diffx, diffy;
			const auto& p = end.at(case_number);
			if (p.first < x)
				diffx = x - p.first;
			else
				diffx = p.first - x;
			if (p.second < y)
				diffy = y - p.second;
			else
				diffy = p.second - y;
			sum += diffx + diffy;
		}
	return sum / 2.;
}

Types::DistType heuristics::linear(const AStarNode& node) noexcept
{
	const auto end = get_end(node.grid.size());
	auto sum = 0.; // return value
	for (auto y = 0u; y < node.grid.size(); y++)
		for (auto x = 0u; x < node.grid.size(); x++) {
			auto case_number = node.grid[y][x];
			if (case_number == 0) continue;
			decltype(x) diffx, diffy;
			const auto& p = end.at(case_number);
			if (p.first < x)
				diffx = x - p.first;
			else
				diffx = p.first - x;
			if (p.second < y)
				diffy = y - p.second;
			else
				diffy = p.second - y;
			sum += std::sqrt((double)(diffx * diffx + diffy * diffy));
		}
	return sum / 2.;
}

Types::DistType heuristics::penalty(const AStarNode& node) noexcept
{
	const auto end = get_end(node.grid.size());
	auto sum = 0u; // return value
	for (auto y = 0u; y < node.grid.size(); y++)
		for (auto x = 0u; x < node.grid.size(); x++) {
			auto case_number = node.grid[y][x];
			if (case_number == 0) continue;
			const auto& p = end.at(case_number);
			if (p.first != x)
				sum++;
			if (p.second != y)
				sum++;
		}
	return sum / 2.;
}
