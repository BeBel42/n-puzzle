//
// Created by Pierre Vanderlynden on 21/06/2023.
//

#include "pipe.hpp"
#include <iostream>
#include <string>
#include "utils.hpp"


using namespace std;


static int lineToInt(string line, int lineNbr) {
    try {
        int e = stoi(line);
        if (e < 0) {
            cout << "Invalid argument on line " << lineNbr << " : " << line << endl;
            exit(0);
        }
        return e;
    } catch (invalid_argument const& ex) {
        cout << "Invalid argument on line " << lineNbr << " : " << line << endl;
        exit(0);
    } catch (out_of_range const& ex) {
        cout << "Invalid argument on line " << lineNbr << " : " << line << endl;
        exit(0);
    }
}

static int skipComments(string& line) {
    int lineNbr = 0;
    while (getline(cin, line)) {
        lineNbr += 1;
        if (!line.empty() && line[0] != '#')
            break;
    }
    return lineNbr;
}

static Types::GridType openAndParse(string firstLine) {
    string line;
    int lineNbr = 1;
    // Pass all comments
    if (!(!firstLine.empty() && firstLine[0] != '#'))
        lineNbr += skipComments(line);
    unsigned int size = lineToInt(line, lineNbr);
    Types::GridType grid;
    for (unsigned int r = 0; r < size; r ++) {
        vector<unsigned int> row;
        for (unsigned int item = 0; item < size; item++) {
            lineNbr += skipComments(line);
            unsigned int x = lineToInt(line, lineNbr);
            if (x >= size * size) {
                cout << "Invalid argument on line " << lineNbr << " : " << line << endl;
                exit(0);
            }
            row.push_back(x);
        }
        grid.push_back(row);
        getline(cin, line);
        lineNbr += 1;
    }

    // Check duplicates
    vector<unsigned int> flattened;
    for (auto const &v: grid) {
        flattened.insert(flattened.end(), v.begin(), v.end());
    }
    sort(flattened.begin(), flattened.end());
    const auto duplicate = std::adjacent_find(flattened.begin(), flattened.end());
    if (duplicate != flattened.end()) {
        std::cout << "Duplicate element = " << *duplicate << endl;
        exit(0);
    }


    // Checks
    Utils::verifyGrid(grid);

    return grid;
}



Types::GridType         Pipe::Parse(string firstLine) {
    Types::GridType grid;
    grid = openAndParse(firstLine);
    return grid;
}
