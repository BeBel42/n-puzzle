//
// Created by Pierre Vanderlynden on 21/06/2023.
//

#ifndef N_PUZZLE_PIPE_H
#define N_PUZZLE_PIPE_H

#include "../../Types.hpp"

namespace Pipe {
    Types::GridType Parse(std::string firstLine);
};


#endif //N_PUZZLE_PIPE_H
