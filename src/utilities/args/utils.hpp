//
// Created by Pierre Vanderlynden on 20/06/2023.
//

#ifndef N_PUZZLE_UTILS_H
#define N_PUZZLE_UTILS_H

#include "../../Types.hpp"

namespace Utils {
    void    printUsage();
    void    verifyGrid(Types::GridType grid);
};


#endif //N_PUZZLE_UTILS_H
