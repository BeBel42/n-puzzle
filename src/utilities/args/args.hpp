//
// Created by Pierre Vanderlynden on 13/06/2023.
//

#ifndef N_PUZZLE_ARGS_H
#define N_PUZZLE_ARGS_H


#include "../../Types.hpp"

namespace Args {
    Types::GridType Parse(int argc, char** argv);

}; // args

#endif //N_PUZZLE_ARGS_H
