//
// Created by Pierre Vanderlynden on 20/06/2023.
//

#include "generate.hpp"
#include "utils.hpp"
#include <iostream>

using namespace std;


unsigned int *swap_empty_x(unsigned int *grid, int s, int n) {
    int idx = 0;
    for (int i = s * s - 1; i > -1; i--) {
        if (grid[i] == 0) {
            idx = i;
            break;
        }
    }
    srand (time(NULL));
    while (n > 0) {
        vector<int> poss;
        if (idx % s > 0) {
            poss.push_back(idx - 1);
        }
        if (idx % s < s - 1) {
            poss.push_back(idx + 1);
        }
        if (idx / s > 0 && idx - s >= 0) {
            poss.push_back(idx - s);
        }
        if (idx / s < s - 1) {
            poss.push_back(idx + s);
        }

        if (poss.size() == 0) {
            break;
        }
        int swi = poss[rand() % poss.size()];
        grid[idx] = grid[swi];
        grid[swi] = 0;
        idx = swi;
        n--;
    }
    return grid;
}

unsigned int *make_target(int s) {
    int ts = s * s;
    int *puzzle = (int *)malloc(sizeof(int) * ts);
    for (int z = 0; z < ts; z++)
        puzzle[z] = -1;
    unsigned cur = 1;
    int x = 0;
    int ix = 1;
    int y = 0;
    int iy = 0;
    while (true) {
        puzzle[x + y * s] = cur;
        if (cur == 0) {
            break;
        }
        cur = cur + 1;
        if ( (x + ix == s) || (x + ix < 0) || (ix != 0 && puzzle[x + ix + y * s] != -1)) {
            iy = ix;
            ix = 0;
        } else if ((y + iy == s) || (y + iy < 0) || (iy != 0 && puzzle[x + (y + iy) * s] != -1)) {
            ix = -iy;
            iy = 0;
        }
        x += ix;
        y += iy;
        if (cur == s*s) {
            cur = 0;
        }
    }
    return (unsigned int *)puzzle;

}

unsigned int *makeItImpossible(unsigned *grid, int s) {
    if (grid[0] == 0 || grid[1] == 0) {
        int end = s*s-1;
        unsigned int tmp = grid[end - 1];
        grid[end - 1] = grid[end];
        grid[end] = tmp;
    } else {
        unsigned int tmp = grid[1];
        grid[1] = grid[0];
        grid[0] = tmp;
    }
    return grid;
}

Types::GridType generate(int size, bool unsolvable) {
    if (size == 1) {
        return {{0}};
    }
    unsigned int *puzzle = swap_empty_x(make_target(size), size, 20);

    if (unsolvable) {
        puzzle = makeItImpossible(puzzle, size);
    }
    Types::GridType grid;
    for (int i = 0; i < size; i++) {
        vector<unsigned> line;
        for (int j = 0; j < size; j++) {
            line.push_back(puzzle[i * size + j]);
        }
        grid.push_back(line);
    }
    return grid;
}


Types::GridType     Generate::Parse(int argc, char** argv) {
    bool unsolvable = false;

    for (int i = 2; i < argc - 1; i++) {
        if ((string)argv[i] == "-u" || (string)argv[i] == "--unsolvable") {
            unsolvable = true;
        } else {
            cout << "Unknown argument: " << argv[i] << endl;
            Utils::printUsage();
        }
    }

    string str_size = argv[argc - 1];
    int size = -1;
    try {
        size = stoi(str_size);
        if (size < 1) {
            cout << "Cannot generate a grid smaller than 1." << endl;
            size = -1;
        }
    } catch (invalid_argument const& ex) {
        size = -1;
    } catch (out_of_range const& ex) {
        size = -1;
    }
    if (size == -1) {
        cout << "Wrong size given as input: " << str_size << endl;
        exit(0);
    }
    return generate(size, unsolvable);
}
