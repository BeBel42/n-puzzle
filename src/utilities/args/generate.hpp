//
// Created by Pierre Vanderlynden on 20/06/2023.
//

#ifndef N_PUZZLE_GENERATE_H
#define N_PUZZLE_GENERATE_H

#include "../../Types.hpp"

namespace Generate {
    Types::GridType Parse(int argc, char** argv);
};


#endif //N_PUZZLE_GENERATE_H
