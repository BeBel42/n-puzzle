//
// Created by Pierre Vanderlynden on 13/06/2023.
//

#include "args.hpp"
#include <iostream>
#include "utils.hpp"
#include "generate.hpp"
#include "file.hpp"
#include "pipe.hpp"

using namespace std;


Types::GridType Args::Parse(int argc, char** argv) {

    if (argc == 1) {
        string input;
        getline(cin, input);
        if (cin.eof()) {
            Utils::printUsage();
        } else {
            return Pipe::Parse(input);
        }
    }
    // generate or read file
    if (argc >= 3) {
        if ((string)argv[1] == "g" || (string)argv[1] == "generate") {
            return Generate::Parse(argc, argv);
        } else if ((string)argv[1] == "f" || (string)argv[1] == "file") {
            return File::Parse(argc, argv);
        }
    }
    Utils::printUsage();
    return Types::GridType();
}