//
// Created by Pierre Vanderlynden on 20/06/2023.
//

#include "utils.hpp"
#include <iostream>

using namespace std;

void            Utils::printUsage() {
    cout << "wrong number of or wrong arguments" << endl;
    cout << "Usage:" << endl;
    cout << "  ./n-puzzle                                       will ask you a size to generate a grid." << endl;
    cout << "  ./n-puzzle g|generate [-u|--unsolvable] SIZE     use a generated grid of size SIZE." << endl;
    cout << "  ./n-puzzle f|file FILENAME                       read a grid from the file FILENAME." << endl;
    exit(0);
}

void            Utils::verifyGrid(Types::GridType grid) {

    // Check duplicates
    vector<unsigned int> flattened;
    for (auto const &v: grid) {
        flattened.insert(flattened.end(), v.begin(), v.end());
    }
    sort(flattened.begin(), flattened.end());
    const auto duplicate = std::adjacent_find(flattened.begin(), flattened.end());
    if (duplicate != flattened.end()) {
        std::cout << "Duplicate element = " << *duplicate << endl;
        exit(0);
    }
}