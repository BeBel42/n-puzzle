//
// Created by Pierre Vanderlynden on 21/06/2023.
//

#include "file.hpp"
#include "utils.hpp"

#include <iostream>
#include <fstream>
#include <fstream>
#include <string>
#include <sstream>

using namespace std;


static inline string            ltrim(std::string s) {
    s.erase(s.begin(), std::find_if(s.begin(), s.end(), [](unsigned char ch) {
        return !std::isspace(ch);
    }));
    return s;
}

static inline string            rtrim(std::string s) {
    s.erase(std::find_if(s.rbegin(), s.rend(), [](unsigned char ch) {
        return !std::isspace(ch);
    }).base(), s.end());
    return s;
}

static inline string            trim(std::string s) {
    rtrim(s);
    ltrim(s);
    return s;
}

static string                   removeComment(string line) {
    size_t index = line.find("#");
    if (index == string::npos) {
        return trim(line);
    }
    return trim(line.substr(0, index));
}

static vector<unsigned int>     parseLine(string line) {
    line = removeComment(line);
    vector<unsigned int> l;

    string s;
    stringstream ss;
    ss << line;

    while (getline(ss, s, ' ')) {
        try {
            int x = stoi(s);
            if (x < 0) {
                cout << "Invalid argument : " << line << endl;
                exit(0);
            }
            l.push_back(x);
        } catch (invalid_argument const& ex) {
            cout << "Invalid argument : " << line << endl;
            exit(0);
        } catch (out_of_range const& ex) {
            cout << "Invalid argument : " << line << endl;
            exit(0);
        }
    }
    return l;
}



Types::GridType         File::Parse(int argc, char** argv) {
    // check for args
    for (int i = 2; i < argc - 1; i++) {
        cout << "Unknown argument: " << argv[i] << endl;
        Utils::printUsage();
    }

    fstream fs;
    fs.open(argv[2], ios::in);
    if (!fs) {
        cout << "File not found." << endl;
        exit(0);
    }
    Types::GridType grid;
    int size = -1;
    string line;
    getline(fs, line);
    // get the size
    while(size == -1) {
        vector<unsigned int> s = parseLine(line);
        if (s.size() > 1) {
            cout << "Invalid argument : " << line << endl;
            exit(0);
        }
        if (s.size() == 1) {
            size = s[0];
        }
        getline(fs, line);
    }
    // get each line
    while(true) {
        vector<unsigned int> row = parseLine(line);
        if (!row.empty()) {
            if(row.size() != (unsigned long)size || find_if(row.begin(), row.end(), [&]( unsigned int const& item ) {
                return item >= size * size; // need to be able to access ID
                // publicly
            }) != row.end()) {
                fs.close();
                cout << "Invalid file." << endl;
                exit(0);
            }
            grid.push_back(row);
        }
        if (fs.eof()) {
            break;
        }
        getline(fs, line);
    }
    fs.close();
    if (grid.size() != (unsigned long)size) {
        cout << "Invalid file." << endl;
        exit(0);
    }

    // Checks
    Utils::verifyGrid(grid);

    return grid;
}