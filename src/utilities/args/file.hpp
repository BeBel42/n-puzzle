//
// Created by Pierre Vanderlynden on 21/06/2023.
//

#ifndef N_PUZZLE_FILE_H
#define N_PUZZLE_FILE_H

#include "../../Types.hpp"

namespace File {
    Types::GridType Parse(int argc, char** argv);
};


#endif //N_PUZZLE_FILE_H
