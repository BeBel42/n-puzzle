#ifndef UTILITIES_H_
#define UTILITIES_H_

#include <iostream>
#include "../Types.hpp"

namespace heuristics {
	// returns distance between node and start
	Types::DistType g(const AStarNode& node) noexcept;
	// return smallest dist estimate between node and end
	// best
	Types::DistType manhattan(const AStarNode& node) noexcept;
	// good
	Types::DistType linear(const AStarNode& node) noexcept;
	// worst
	Types::DistType penalty(const AStarNode& node) noexcept;
}

#endif // UTILITIES_H_
