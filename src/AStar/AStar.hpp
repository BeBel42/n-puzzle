#ifndef ASTAR_H_
#define ASTAR_H_

#include <iostream>
#include "../Types.hpp"
#include "../AStarNode/AStarNode.hpp"

// A Star algorithm
class AStar {
	public:
		// launches the astar algorithm
		AStar(const Types::GridType& start,
		      const Types::Heuristic& f_h,
		      const Types::Heuristic& f_g) noexcept;
	private:
		// passed to ctor
		Types::sptr_t start_node;
		// end node of solution
		Types::sptr_t end_node = nullptr;
		friend std::ostream& operator<<(std::ostream&, const AStar&) noexcept;
		std::size_t n_visited;
		std::size_t n_created;
		double time_taken;
};

// diplays move list and other benchmarks
std::ostream& operator<<(std::ostream&, const AStar&) noexcept;

#endif // ASTAR_H_
