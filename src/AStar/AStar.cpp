#include "AStar.hpp"
#include "../Types.hpp"
#include <algorithm>
#include <chrono>

static void apply_is_a(const Types::sptr_t& node,
							  Types::NeighborSides is_a) noexcept
{
	const auto dim = node->grid.size();
	auto x = 0u;
	auto y = 0u;
	while (y < dim) {
		x = 0;
		while (x < dim) {
			if (node->grid[y][x] == 0) break;
			x++;
		}
		if (x != dim) break;
		y++;
	}
	switch (is_a) {
		case Types::NeighborSides::LEFT:
			std::swap(
				node->grid[y][x],
				node->grid[y][x - 1]
			);
			break;
		case Types::NeighborSides::RIGHT:
			std::swap(
				node->grid[y][x],
				node->grid[y][x + 1]
			);
			break;
		case Types::NeighborSides::UP:
			std::swap(
				node->grid[y][x],
				node->grid[y - 1][x]
			);
			break;
		case Types::NeighborSides::DOWN:
			std::swap(
				node->grid[y][x],
				node->grid[y + 1][x]
			);
			break;
		default:
			break;
	}
}

AStar::AStar(const Types::GridType& start,
			 const Types::Heuristic& f_h,
			 const Types::Heuristic& f_g) noexcept
	: start_node(std::make_shared<AStarNode>(start))
{
	// benchmark variable
	const auto t1 = std::chrono::steady_clock::now();
	// initialize start node
	start_node->g = 0;
	start_node->h = f_h(*start_node);
	start_node->this_ptr = start_node;
	start_node->parent = nullptr;
	// computing astar nodes
	Types::PendingNodes pending_nodes;
	Types::Visited visited;
	auto begin = start_node;
	while (begin->h) {
		begin->generate_neighbors(pending_nodes, visited, f_h, f_g);
		visited.insert(begin);
		if (pending_nodes.empty()) break;
		begin = std::move(pending_nodes.top());
		pending_nodes.pop();
	}
	// check if solution found
	if (!pending_nodes.empty() || begin == start_node)
		end_node = std::move(begin);
	// +1 because begin is not directly added to visited
	n_visited = visited.size() + 1;
	n_created = n_visited + pending_nodes.size();
	// benchmark computing
	const auto t2 = std::chrono::steady_clock::now();
	const std::chrono::duration<double> diff = t2 - t1;
	time_taken = diff.count();
}

// recursively tracerse list and display grids
static void display_moves_r(std::ostream& os, const auto& end_node,
							auto& start_node, auto&& count) noexcept
{
	if (!end_node->parent)
		return;
	display_moves_r(os, end_node->parent, start_node, count);
	count++;
	apply_is_a(start_node, end_node->is_a);
	os << count << ":\n";
	os << *end_node << '\n';
}

std::ostream& operator<<(std::ostream& os, const AStar& astar) noexcept
{
	const auto delimiter = std::string(
		astar.start_node->grid.size() * 2 - 1,'=');
	std::cout << delimiter << '\n';
	std::cout << *astar.start_node;
	std::cout << delimiter << "\n\n";
	if (!astar.end_node)
		os << "No solution possible\n";
	else
		display_moves_r(os, astar.end_node, astar.start_node, 0);
	os << "N of visited nodes: " << astar.n_visited << '\n';
	os << "N of created nodes: " << astar.n_created << '\n';
	os << "Time taken by algorithm: " << astar.time_taken << "s\n";
	return os;
}
