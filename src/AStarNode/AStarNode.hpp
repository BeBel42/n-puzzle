#ifndef ASTARNODE_H_
#define ASTARNODE_H_

#include "../Types.hpp"
#include <functional>

// class representing node of astar graph
class AStarNode
{
	private:
		// creates side neighbor and adds it to nodes list
		void create_neighbor(const Types::sptr_t& neigh,
							 const Types::NeighborSides side,
							 const unsigned max_x,
							 const unsigned max_y,
							 Types::PendingNodes& pending_nodes,
							 const Types::Visited& visited,
							 const Types::Heuristic& f_h,
							 const Types::Heuristic& f_g) const noexcept;
	public:
		// is [left|right|...] neighbor of parent
		Types::NeighborSides is_a = Types::NeighborSides::NONE;
		// to set neighbor's parent
		Types::wptr_t this_ptr;
		// grid represented by node
		Types::GridType grid;
		// cached h()
		Types::DistType h;
		// cached g()
		Types::DistType g;
		// preceding node for shortest path
		Types::sptr_t parent;

		// generate neighbors and put them in nodes list
		void generate_neighbors(Types::PendingNodes& pending_nodes,
								const Types::Visited& visited,
								const Types::Heuristic& f_h,
								const Types::Heuristic& f_g) const noexcept;

		// ctor
		template <class T>
		AStarNode(T&& v) noexcept
			: grid(std::forward<T>(v)) {}
};

// to store them in hash tables as ptrs
namespace std {
	template <> struct hash<Types::sptr_t> {
		size_t operator()(const Types::sptr_t& node) const noexcept;
	};
}

// to compare them as ptrs (set.includes())
bool operator==(const Types::sptr_t& a, const Types::sptr_t& b) noexcept;

// to sort them
bool operator<(const Types::sptr_t& a, const Types::sptr_t& b) noexcept;
bool operator>(const Types::sptr_t& a, const Types::sptr_t& b) noexcept;
bool operator<=(const Types::sptr_t& a, const Types::sptr_t& b) noexcept;
bool operator>=(const Types::sptr_t& a, const Types::sptr_t& b) noexcept;

// output to console
std::ostream& operator<<(std::ostream&, const AStarNode& node) noexcept;

#endif // ASTARNODE_H_
