#include <algorithm>
#include <boost/functional/hash.hpp>
#include "../Types.hpp"
#include "AStarNode.hpp"

void AStarNode::create_neighbor(const Types::sptr_t& neigh,
								const Types::NeighborSides side,
								const unsigned max_x,
								const unsigned max_y,
								Types::PendingNodes& pending_nodes,
								const Types::Visited& visited,
								const Types::Heuristic& f_h,
								const Types::Heuristic& f_g) const noexcept {
	switch (side) {
		case Types::NeighborSides::DOWN:
			std::swap(
				neigh->grid[max_y][max_x],
				neigh->grid[max_y + 1][max_x]
				);
				break;
		case Types::NeighborSides::UP:
			std::swap(
				neigh->grid[max_y][max_x],
				neigh->grid[max_y - 1][max_x]
				);
				break;
		case Types::NeighborSides::LEFT:
			std::swap(
				neigh->grid[max_y][max_x],
				neigh->grid[max_y][max_x - 1]
				);
				break;
		case Types::NeighborSides::RIGHT:
			std::swap(
				neigh->grid[max_y][max_x],
				neigh->grid[max_y][max_x + 1]
				);
				break;
		default:
			;// do onthing
		}
	if (visited.contains(neigh)) return;
	neigh->g = f_g(*this) + 1;
	neigh->h = f_h(*neigh);
	neigh->parent = this_ptr.lock();
	neigh->this_ptr = neigh;
	pending_nodes.emplace(neigh);
	neigh->is_a = side;
}

void AStarNode::generate_neighbors(Types::PendingNodes& pending_nodes,
								   const Types::Visited& visited,
								   const Types::Heuristic& f_h,
								   const Types::Heuristic& f_g) const noexcept
{
	// finding "empty" position
	const auto dim = grid.size();
	auto max_y = 0u;
	auto max_x = 0u;
	while (max_y < dim) {
		max_x = 0u;
		while (max_x < dim) {
			if (grid[max_y][max_x] == 0) break;
			max_x++;
		}
		if (max_x != dim) break;
		max_y++;
	}
	using ns = Types::NeighborSides;
	// binding function to avoid clutter;
	const auto lambda = [max_x, max_y, &pending_nodes,
						 &visited, this, &f_h, &f_g](const ns side)
	{
		create_neighbor(std::make_shared<AStarNode>(grid), side,
						max_x, max_y, pending_nodes, visited, f_h, f_g);
	};
	// generating neighbor if situation permits it
	if (is_a != ns::RIGHT && max_x > 0)
		lambda(ns::LEFT);
	if (is_a != ns::LEFT && max_x < dim - 1)
		lambda(ns::RIGHT);
	if (is_a != ns::UP && max_y < dim - 1)
		lambda(ns::DOWN);
	if (is_a != ns::DOWN && max_y > 0)
		lambda(ns::UP);
}

std::size_t std::hash<Types::sptr_t>::operator()(
	const Types::sptr_t& node) const noexcept
{
	std::size_t hash = 0;
	for (const auto& i : node->grid) {
		const auto hashr = boost::hash_range(
			i.begin(), i.end());
		boost::hash_combine(hash, hashr);
	}
	return hash;
}

bool operator==(const Types::sptr_t& a, const Types::sptr_t& b) noexcept {
	return std::hash<Types::sptr_t>()(a) == std::hash<Types::sptr_t>()(b);
}

bool operator<(const Types::sptr_t& a, const Types::sptr_t& b) noexcept {
	return a->h + a->g < b->h + b->g;
}
bool operator>(const Types::sptr_t& a, const Types::sptr_t& b) noexcept {
	return a->h + a->g >= b->h + b->g;
}
bool operator<=(const Types::sptr_t& a, const Types::sptr_t& b) noexcept {
	return a->h + a->g < b->h + b->g;
}
bool operator>=(const Types::sptr_t& a, const Types::sptr_t& b) noexcept {
	return a->h + a->g >= b->h + b->g;
}

std::ostream& operator<<(std::ostream& o, const AStarNode& node) noexcept
{
	for (const auto& i : node.grid) {
		for (const auto& j : i) {
			if (j == 0)
				o << ' ' << ' ';
			else
				o << j << ' ';
		}
		o << '\n';
	}
	return o;
}
