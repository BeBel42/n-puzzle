#include <iostream>
#include "AStar/AStar.hpp"
#include "Types.hpp"
#include "utilities/heuristics.hpp"
#include "utilities/args/args.hpp"

// small unit tests
void short_tests(const auto& h, const auto& g)
{
	std::cout << "possible 2x2\n";
	std::cout << AStar({
		{2, 3},
		{0, 1}
	}, h, g);

	std::cout << "impossible 2x2\n";
	std::cout << AStar({
		{2, 0},
		{3, 1}
	}, h, g);

	std::cout << "1x1\n";
	std::cout << AStar(Types::GridType {
		{0},
	}, h, g);

	std::cout << "possible 3x3\n";
	std::cout << AStar({
		{6, 1, 4},
		{3, 0, 8},
		{2, 5, 7}
	}, h, g);
}

void slow_tests(const auto& h, const auto& g)
{
	std::cout << "impossible 3x3\n";
	std::cout << AStar({
		{7, 6, 8},
		{5, 2, 1},
		{0, 4, 3}
	}, h, g);

	std::cout << "possible 4x4\n";
	std::cout << AStar({
		{ 1,  3,  14,  0},
		{ 12, 15, 5,  4},
		{10,  11, 6,  2},
		{9, 13,  7,  8},
	}, h, g);
}

int main(int argc, char** argv)
{
    Types::GridType grid = Args::Parse(argc, argv);

	auto g = heuristics::g;
	auto h = heuristics::manhattan;

    std::cout << AStar(grid, h, g);

	// short_tests(h, g);
	// h = heuristics::linear;
	// short_tests(h, g);
	// h = heuristics::penalty;
	// short_tests(h, g);
}
