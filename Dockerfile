FROM trafex/php-nginx
USER root
WORKDIR /app
RUN apk update && apk add boost-dev make g++ gcompat
COPY . .
RUN make
RUN mv web/* /var/www/html/
USER nobody

ENV N_PUZZLE_PATH=/app/n-puzzle

EXPOSE 8080
